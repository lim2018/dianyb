import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

Vue.prototype.$serverUrl = 'http://movie.m616.top';

Vue.prototype.$pictureUrl = 'http://movie.m616.top';

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
